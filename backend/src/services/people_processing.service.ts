import people_data from "../data/people_data.json";

type PersonName = {
  firstName: string;
  lastName: string;
};

type Person = {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  title: string;
  company: string;
  avatar: string;
};

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getByName(fullName: PersonName) {
    return people_data.filter((people) => {
      const hasSameName =
        people.first_name.toLocaleLowerCase() === fullName.firstName;
      const hasSameLastName =
        people.last_name.toLocaleLowerCase() === fullName.lastName;

      return hasSameName || hasSameLastName;
    });
  }

  getAll() {
    return people_data;
  }
}

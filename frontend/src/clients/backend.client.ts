import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(): Promise<{ data: IUserProps[] }> {
    const response = await axios.request({
      url: `${this.baseUrl}/people/all`,
      method: "GET",
    });
    return response.data;
  }
}
